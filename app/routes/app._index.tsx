import type { ActionFunctionArgs, LoaderFunctionArgs } from "@remix-run/node";
import { defer } from "@remix-run/node";
import {
  useActionData,
  useLoaderData,
  useNavigate,
  useNavigation,
  useSubmit,
} from "@remix-run/react";
import {
  Page,
  Layout,
  Text,
  Card,
  BlockStack,
  IndexTable,
  Thumbnail,
  EmptySearchResult,
  SkeletonBodyText,
  SkeletonThumbnail,
  PageActions,
  Button,
} from "@shopify/polaris";
import { authenticate } from "../shopify.server";
import type { QueryProductsResponse } from "~/types/demo";
import indexStyles from "../styles/demo-style.css";
import { URLS } from "./app";
import { useEffect, useState } from "react";
import { TAG_ID } from "./app.new-product"

export const links = () => [{ rel: "stylesheet", href: indexStyles }];

export const action = async ({ request }: ActionFunctionArgs) => {
  const formData = await request.formData();
  const action = formData.get("_action");
  if(action){
    const next = formData.get("next")?.toString()
    const { admin } = await authenticate.admin(request);
    const response = await admin.graphql(
      `#graphql
      query ($next: String, $query: String){
        products(reverse: true, first: 10, after: $next, query: $query) {
          edges {
            cursor
            node {
              title,
              description,
              images(first: 1) {
                edges {
                  node {
                    url,
                    altText,
                  }
                }
              }
            }
          }
          pageInfo {
            hasNextPage
            endCursor
          }
        }
      }`,
      {
        variables: {
          next: next,
          query: `tag:"${TAG_ID}"`,
        },
      },
    );
    const {data} = await response.json() as QueryProductsResponse;
    return data;
  }
  return null
};

export const loader = async ({ request }: LoaderFunctionArgs) => {
  const { admin } = await authenticate.admin(request);
  const response = admin.graphql(
    `#graphql
    query ($query: String) {
      products(reverse: true, first: 10, query: $query) {
        edges {
          cursor
          node {
            title,
            description,
            images(first: 1) {
              edges {
                 node {
                  url,
                  altText,
                 }
              }
            }
          }
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }`,
    {
      variables: {
        query: `tag:"${TAG_ID}"`,
      },
    },
  );
  return defer({ dataPromise: response.then((res) => res.json()) });
};

export default function Index() {
  const submit = useSubmit();
  const navigate = useNavigate();
  const nav = useNavigation();
  const actionData = useActionData<typeof action>();
  
  const loaderPromise = useLoaderData<typeof loader>();
  const [isLoaderLoaded, setIsLoaderLoaded] = useState(false)
  const isSubmitting = nav.state !== "idle" && nav.formMethod === "POST"
  const [isLoading, setIsLoading] = useState(true)
  const [nextPage, setNextPage] = useState<string>("")
  const [products, setProducts] = useState<QueryProductsResponse["data"]["products"]["edges"] | []>([])
  useEffect (() => {
    if(isLoaderLoaded) return

    (async () => {
      setIsLoading(true);
      const loader: QueryProductsResponse = await loaderPromise.dataPromise.then(res => res)
      setProducts(products.length ? [...products, ...loader.data.products.edges] : [...loader.data.products.edges])
      if(loader.data.products.pageInfo.hasNextPage == false) {
        setNextPage("")
      } else setNextPage(loader.data.products.pageInfo.endCursor || "");

      setIsLoading(false);
      setIsLoaderLoaded(true)
    })()
  }, [loaderPromise, isLoaderLoaded])

  useEffect(() => {
    if(!actionData) return

    setProducts(products.length ? [...products, ...actionData.products.edges] : [...actionData.products.edges])
    if(actionData.products.pageInfo.hasNextPage == false) {
      setNextPage("")
    } else setNextPage(actionData.products.pageInfo.endCursor || "");
  },[actionData])


  return (
    <Page>
      <ui-title-bar title="Dashboard">
        <button
          variant="primary"
          onClick={() => {
            navigate(`${URLS["new-product"]}`);
          }}
        >
          Create new product
        </button>
      </ui-title-bar>
      <BlockStack gap="500">
        <Layout>
          <Layout.Section>
            <Text variant="heading3xl" as="h2">
              👾 Products
            </Text>
            <Card padding="0">
              <IndexTable
                emptyState={emptyStateMarkup}
                headings={[
                  { title: "" },
                  { title: "Title", alignment: "start" },
                  { title: "Description", alignment: "start" },
                ]}
                itemCount={isLoading ? 10 + products.length : products.length}
                selectable={false}
              >

                {products?.length ?
                  <>
                    {products.map((product, idx) => (
                      <TableRow idx={idx} product={product} key={product.cursor}/>
                    ))}
                  </> : null
                }
                {(isLoading || isSubmitting) && <SkeletonProducts />}
              </IndexTable>
            </Card>
            {(nextPage !== "") &&
            <PageActions primaryAction={
              <Button disabled={isLoading || isSubmitting} variant="primary" onClick={handleLoadingProducts}>
                  Load more
              </Button>}
            />}
          </Layout.Section>
        </Layout>
      </BlockStack>
    </Page>
  );

  function handleLoadingProducts(){
    const form = new FormData();
    form.append("_action", "NEXT_PAGE");
    
    if(!nextPage) throw new Error ("Something went wrong.")
    form.append("next", nextPage);
    submit(form, { method: "POST"})
  }
}

const emptyStateMarkup = (
  <EmptySearchResult
    title={"No entries yet"}
    description={"Create a new product first"}
    withIllustration
  />
);

const SkeletonProducts = () => (
  <>
    {Array.from({ length: 10 }).map((_, idx) => (
      <IndexTable.Row id={idx.toString()} key={idx} position={idx}>
        <IndexTable.Cell>
          {" "}
          <SkeletonThumbnail size="medium" />{" "}
        </IndexTable.Cell>
        <IndexTable.Cell className="cell-title">
          <SkeletonBodyText lines={1} />{" "}
        </IndexTable.Cell>
        <IndexTable.Cell className="cell-description">
          <SkeletonBodyText lines={3} />{" "}
        </IndexTable.Cell>
      </IndexTable.Row>
    ))}
  </>
);

const TableRow = ({product, idx}: {product: QueryProductsResponse["data"]["products"]["edges"][0], idx: any}) => (
    <IndexTable.Row
      id={product.cursor}
      key={product.cursor}
      position={idx}
    >
      <IndexTable.Cell>
        <Thumbnail
          source={(product.node.images.edges.length && product.node.images.edges[0].node?.url) || ""}
          alt={(product.node.images.edges.length && product.node.images.edges[0].node?.altText) ||""}
        />
      </IndexTable.Cell>
      <IndexTable.Cell className="cell-title">
        {product.node.title}
      </IndexTable.Cell>
      <IndexTable.Cell className="cell-description">
        {product.node.description || "No description."}
      </IndexTable.Cell>
    </IndexTable.Row>
)