import { unstable_createMemoryUploadHandler, type ActionFunctionArgs, unstable_parseMultipartFormData } from "@remix-run/node";
import { useActionData, useNavigation, useSubmit } from "@remix-run/react";
import {
  Button,
  ButtonGroup,
  Card,
  FormLayout,
  InlineError,
  Layout,
  Page,
  TextField,
} from "@shopify/polaris";
import type { ChangeEvent } from "react";
import { useEffect, useMemo, useRef, useState } from "react";
import { authenticate } from "~/shopify.server";
import { CancelMinor } from "@shopify/polaris-icons";
import type { StageImageResponse } from "~/types/demo";

const MAX_SIZE = 20 * 1024 * 1024; // max 20MB
const FORM_UPLOAD = "Form uploaded";
const IMAGE_UPLOAD = "Image uploaded";
const IMAGE_REMOVE = "Image removed";
export const TAG_ID = "Custom";

type Product = {
  title: string;
  description: string;
  tags: string;
  metakey: string;
  metavalue: string;
};

type Variables = {
  input: {
    title: string;
    descriptionHtml: string;
    tags: string[];
    metafields: {
      key: string;
      value: string;
      namespace: string;
      type: string;
    }[];
  }
  media?: {
    originalSource: string;
    alt: string;
    contentType: string;
  }[];
}

const emptyProduct = {
  title: "",
  description: "",
  tags: "",
  metakey: "",
  metavalue: "",
};

export const action = async ({ request }: ActionFunctionArgs) => {
  
  const uploadHandler = unstable_createMemoryUploadHandler({
    maxPartSize: MAX_SIZE
  });
  const formData = await unstable_parseMultipartFormData(
    request,
    uploadHandler
  );
  const image = formData.get("file") as File;
  const { admin } = await authenticate.admin(request);

  let isImageUploaded: boolean = false
  let parameters, resourceUrl, url
  if (image) {
    try {
      // 1* STEP: Stage image bucket
      const stageImage = await admin.graphql(
        `#graphql
          mutation stagedUploadsCreate($input: [StagedUploadInput!]!) {
            stagedUploadsCreate(input: $input) {
              stagedTargets {
                url
                resourceUrl
                parameters {
                  name
                  value
                }
              }
              userErrors {
                field
                message
              }
            }
          }`,
        {
          variables: {
            input: [
              {
                filename: image.name,
                fileSize: image.size.toString(),
                mimeType: image.type,
                httpMethod: "POST",
                resource: "IMAGE",
              },
            ],
          },
        },
      );
      const stageImageJson = await stageImage.json() as StageImageResponse;

      // parameters to interact with the aws bucket.
      // url to post data to aws. It's a generic s3 url that when combined with the params sends the data to the right place.
      // resourceUrl is the specific url that will contain the image data after uploading the file to the aws staged target.
      ({ parameters, resourceUrl, url } = stageImageJson.data.stagedUploadsCreate.stagedTargets[0]);

      const form = new FormData();
      parameters.forEach(({ name, value }) => {
        form.append(name, value);
      });
      form.append("file", image)
      form.append("url", url)
      form.append("resourceUrl", resourceUrl);

      // 2* STEP: Upload image asset
      await fetch(url, {
        method: "POST",
        body: form,

      }).then((res) => {
        console.error("let's check res")
        console.error(res)
        if (!res.ok) throw new Error("Couldn't upload image");
      })
      await admin.graphql(
        `#graphql
        mutation fileCreate($files: [FileCreateInput!]!) {
          fileCreate(files: $files) {
            files {
              alt
            }
            userErrors {
              field
              message
            }
          }
        }`,
        {
          variables: {
            files: [
              {
                originalSource: resourceUrl,
                alt: `product-image-${image.name}`,
                contentType: "IMAGE",
              },
            ],
          },
        },
      );
      isImageUploaded = true;
    } catch (err) {
      console.error(err);
      isImageUploaded = false;
    }
  }

  // Upload form
  try {
    let mutation;
    const 
      title = formData.get("title")?.toString() || "No title", // Shouldn't trigger at all?
      description = formData.get("description")?.toString() || "",
      tags = formData.get("tags")?.toString().split(" ").concat([TAG_ID]) || [TAG_ID],
      metakey = formData.get("metakey")?.toString() || "",
      metavalue = formData.get("metavalue")?.toString() || "";
    
      const variables: Variables = {
        input: {
          title: title,
          descriptionHtml: `<p>${description}</p>`,
          tags: tags,
          metafields: [
            {
              key: metakey,
              value: metavalue,
              namespace: "custom_metafield",
              type: "single_line_text_field",
            },
          ],
        },
      };
      if (isImageUploaded){
        mutation = `#graphql
          mutation CreateProductWithMetafieldsAndMedia($input: ProductInput!, $media: [CreateMediaInput!]) {
          productCreate(input: $input, media: $media) {
            product {
              id
              title
              descriptionHtml
              tags
              metafields(first: 3) {
                edges {
                  node {
                    id
                    namespace
                    key
                    value
                  }
                }
              }
              media(first: 10) {
                nodes {
                  alt
                  mediaContentType
                  preview {
                    status
                  }
                }
              }
            }
            userErrors {
              field
              message
            }
          }
        }`;
        variables.media = [
          {
            originalSource: resourceUrl,
            alt: `product-image-${image.name}`,
            mediaContentType: "IMAGE",
          }
        ];
      } else {
        mutation = `#graphql
          mutation CreateProductWithMetafieldsAndMedia($input: ProductInput!) {
          productCreate(input: $input) {
            product {
              id
              title
              descriptionHtml
              tags
              metafields(first: 3) {
                edges {
                  node {
                    id
                    namespace
                    key
                    value
                  }
                }
              }
            }
            userErrors {
              field
              message
            }
          }
        }`
      }
      const response = await admin.graphql(mutation, {variables})
      const responseJSON = await response.json();
      return { ...responseJSON, query: FORM_UPLOAD };
  } catch (err) {
    return err
  }
};

export default function ProductForm() {
  const submit = useSubmit();
  const actionData = useActionData<typeof action>();
  const navigation = useNavigation();
  const isLoading = navigation.state !== "idle";

  const [isError, setIsError] = useState<string | null>(null)
  const [formState, setFormState] = useState<Product>(emptyProduct);
  const [selectedImage, setSelectedImage] = useState<File | null | undefined>(undefined);
  const fileInputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    let message;
    if (selectedImage) {
      message = IMAGE_UPLOAD;
    } else if (selectedImage === null) {
      message = IMAGE_REMOVE;
    } else if (actionData && actionData.query) {
      message = actionData.query;
    }
    message && shopify.toast.show(message);
  }, [selectedImage, actionData]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const inputKey = useMemo(() => Date.now(), [selectedImage]);

  return (
    <Page>
      <ui-title-bar title="New product" />
      <Layout>
        <Layout.AnnotatedSection 
          id="storeDetails"
          title="Create a new product"
          description="Fill the form then upload by clicking save."
        >
          <Card background="bg">
            <FormLayout>
              <div style={{
                display: "flex",
                alignItems: "center",
                flexDirection: "row-reverse",
                justifyContent: "space-between",
                WebkitJustifyContent: "space-between"
                }}>
                <Button variant="primary" onClick={handleSave} disabled={isLoading}>
                  Save
                </Button>
                {isError &&
                <InlineError message={isError} fieldID="titleError" />
                }
              </div>
              <TextField
                type="text"
                label="Product Title"
                placeholder="Insert product title"
                value={formState.title}
                onChange={(title) => setFormState({ ...formState, title })}
                autoComplete="off"
              />
              <TextField
                type="text"
                multiline={4}
                label="Product description"
                placeholder="Insert product description"
                value={formState.description}
                onChange={(description) =>
                  setFormState({ ...formState, description })
                }
                autoComplete="off"
              />
              <TextField
                type="text"
                label="Product tags"
                placeholder="Insert a comma separated list of tags"
                value={formState.tags}
                onChange={(tags) => setFormState({ ...formState, tags })}
                autoComplete="off"
              />
              <FormLayout.Group>
                <TextField
                  type="text"
                  label="Metafield key"
                  value={formState.metakey}
                  onChange={(metakey) =>
                    setFormState({ ...formState, metakey })
                  }
                  autoComplete="off"
                />
                <TextField
                  type="text"
                  label="Metafield value"
                  value={formState.metavalue}
                  onChange={(metavalue) =>
                    setFormState({ ...formState, metavalue })
                  }
                  autoComplete="off"
                />
              </FormLayout.Group>
              <input
                style={{display: "none"}}
                type="file"
                accept="image/jpeg, image/png"
                alt="submit image"
                key={inputKey}
                ref={fileInputRef}
                onChange={handleImageChange}
              />
                {selectedImage ? (
                  <ButtonGroup variant="segmented">
                    <Button variant="primary" onClick={handleInputClick} disabled={isLoading}>
                      Change image
                    </Button>
                    <Button
                      variant="primary"
                      onClick={() => setSelectedImage(null)}
                      icon={CancelMinor}
                      accessibilityLabel="Remove image"
                      disabled={isLoading}
                    />
                  </ButtonGroup>
                ) : (
                  <Button variant="primary" onClick={handleInputClick}>
                    Upload image
                  </Button>
                )}
                
            </FormLayout>
          </Card>
        </Layout.AnnotatedSection>
      </Layout>
    </Page>
  );

  function handleInputClick() {
    fileInputRef.current?.click();
  }

  function handleImageChange(event: ChangeEvent<HTMLInputElement>) {
    const file = event.target.files && (event.target.files[0] as File);
    if (file) {
      if (file.size > MAX_SIZE) {
        event.target.value = ""; // Clear the input field
        shopify.toast.show("Image is too big (Max 20MB)", { isError: true });
      } else { // Correct image
        setSelectedImage(file);
      }
    }
  }

  function handleSave() {

    // input validation
    const errors = {
      title: formState.title ? null : "Title is required",
      metakey: !formState.metavalue ? null : formState.metakey.length >= 3 ? null : "Key is too short (minimum 3 characters)",
    };

    for (const [key, value] of Object.entries(errors)) {
      if (value !== null) {
        setIsError(value);
        return;
      }
    };
    setIsError(null)

    const form = new FormData();
    selectedImage && form.append("file", selectedImage);
    for (const [key, value] of Object.entries(formState)) {
      form.append(key, value);
    }
    submit(form, {replace: true, method: "POST", encType: "multipart/form-data"});
    setFormState({ ...emptyProduct });
  }

}
