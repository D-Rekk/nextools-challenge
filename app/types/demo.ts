type StagedTargetParameter = {
  name: string;
  value: string;
};

type StagedTarget = {
  url: string;
  resourceUrl: string;
  parameters: StagedTargetParameter[];
};

export type StageImageResponse = {
  data: {
    stagedUploadsCreate: {
      stagedTargets: StagedTarget[];
      userErrors: unknown[];
    };
  };
};

export type ProductCreateResponse = {
  data: {
    fileCreate: {
      files: {
        alt: string;
      }[];
      userErrors: unknown[];
    };
  };
};

export type QueryProductsResponse = {
  data: {
    products: {
      edges: {
        cursor: string;
        node: {
          title: string;
          description?: string;
          images: {
            edges: {
              node?: {
                url: string;
                altText: string;
              };
            }[];
          };
        };
      }[];
      pageInfo: {
        hasNextPage: boolean;
        endCursor?: string;
      }
    };
  };
}
